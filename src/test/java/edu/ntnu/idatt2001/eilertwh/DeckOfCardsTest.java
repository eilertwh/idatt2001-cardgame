package edu.ntnu.idatt2001.eilertwh;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

    @Nested
    public class TestDealHand {

        @Test
        public void TestExceptionThrownIfDealEmptyHand() {
            DeckOfCards deck = new DeckOfCards();
            try {
                ArrayList<PlayingCard> cards = deck.dealHand(0);
                fail();
            } catch (IllegalAccessException e) {
                assertEquals("you cant deal an empty hand", e.getMessage());
            }
        }

        @Test
        public void TestExceptionThrownIfDealNegativeHand() {
            DeckOfCards deck = new DeckOfCards();
            try {
                ArrayList<PlayingCard> cards = deck.dealHand(-1);
                fail();
            } catch (IllegalAccessException e) {
                assertEquals("you cant have a negative amount of cards", e.getMessage());
            }
        }

        @Test
        public void TestExceptionThrownIfDealTooBigHand() {
            DeckOfCards deck = new DeckOfCards();
            try {
                ArrayList<PlayingCard> cards = deck.dealHand(53);
                fail();
            } catch (IllegalAccessException e) {
                assertEquals("not enough cards in deck", e.getMessage());
            }
        }

        @Test
        public void TestDealHandAmount() {
            DeckOfCards deck = new DeckOfCards();
            ArrayList<PlayingCard> cardsOne = null;
            ArrayList<PlayingCard> cardsTwo = null;
            try {
                cardsOne = deck.dealHand(6);
            } catch (IllegalAccessException e) {
                fail();
            }
            try {
                cardsTwo = deck.dealHand(8);
            } catch (IllegalAccessException e) {
                fail();
            }
            assertEquals(6, cardsOne.size());
            assertEquals(8, cardsTwo.size());
        }

        @Test
        public void TestDealAllUniqueCards() {
            DeckOfCards deck = new DeckOfCards();

            try {
                assertFalse(deck.dealHand(52).stream()
                        .map(c -> c.getAsString())
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                        .entrySet()
                        .stream().anyMatch(element -> element.getValue() > 1));
            } catch (IllegalAccessException e) {
                fail();
            }
        }
    }
}
