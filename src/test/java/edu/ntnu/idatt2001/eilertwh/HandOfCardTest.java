package edu.ntnu.idatt2001.eilertwh;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardTest {

    @Nested
    public class TestGetSum{

        @Test
        public void testGetSumWhenHandHasCards(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('H', 4)));
            HandOfCards hand = new HandOfCards(cards);

            assertEquals(22,hand.getSum());
        }

        @Test
        public void testGetSumWhenHandHasNoCards(){
            HandOfCards hand = new HandOfCards(new ArrayList<>());

            assertEquals(0,hand.getSum());
        }
    }

    @Nested
    public class TestGetSuitCards{

        @Test
        public void TestGetHeartCards(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('H', 4)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> hearts = hand.getSuitCards('H');

            boolean allHeats = hearts.stream().noneMatch(c -> c.getSuit() != 'H');

            assertEquals(2,hearts.size());
            assertTrue(allHeats);
        }

        @Test
        public void TestGetHeartCardsWhenNoHeatsInHand(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('C', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('S', 8)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> hearts = hand.getSuitCards('H');

            assertEquals(0,hearts.size());
        }

        @Test
        public void TestGetClubsCards(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('C', 4)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> clubs = hand.getSuitCards('C');

            boolean allClubs = clubs.stream().noneMatch(c -> c.getSuit() != 'C');

            assertEquals(2,clubs.size());
            assertTrue(allClubs);
        }

        @Test
        public void TestGetClubsCardsWhenNoClubsInHand(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('S', 8),
                            new PlayingCard('H', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('S', 9)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> clubs = hand.getSuitCards('C');

            assertEquals(0,clubs.size());
        }

        @Test
        public void TestGetDiamondsCards(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('D', 4)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> Diamonds = hand.getSuitCards('D');

            boolean allClubs = Diamonds.stream().noneMatch(c -> c.getSuit() != 'D');

            assertEquals(2,Diamonds.size());
            assertTrue(allClubs);
        }

        @Test
        public void TestGetDiamondsCardsWhenNoDiamondsInHand(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('C', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 7),
                            new PlayingCard('H', 3),
                            new PlayingCard('S', 4)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> clubs = hand.getSuitCards('D');

            assertEquals(0,clubs.size());
        }

        @Test
        public void TestGetSpadesCards(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 5),
                            new PlayingCard('D', 3),
                            new PlayingCard('S', 4)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> spades = hand.getSuitCards('S');

            boolean allClubs = spades.stream().noneMatch(c -> c.getSuit() != 'S');

            assertEquals(2,spades.size());
            assertTrue(allClubs);
        }

        @Test
        public void TestGetSpadesCardsWhenNoSpadesInHand(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('C', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('H', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('H', 8)));
            HandOfCards hand = new HandOfCards(cards);

            ArrayList<PlayingCard> clubs = hand.getSuitCards('S');

            assertEquals(0,clubs.size());
        }
    }

    @Nested
    public class TestContainsQueenOfSpades{

        @Test
        public void TestContainsQueenOfSpadesWhenHandContainsQueenOfSpades(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 12),
                            new PlayingCard('D', 3),
                            new PlayingCard('H', 4)));
            HandOfCards hand = new HandOfCards(cards);

            assertTrue(hand.containsQueenOfSpades());
        }

        @Test
        public void TestContainsQueenOfSpadesWhenHandDoesNotContainsQueenOfSpades(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('S', 11),
                            new PlayingCard('D', 3),
                            new PlayingCard('H', 4)));
            HandOfCards hand = new HandOfCards(cards);

            assertFalse(hand.containsQueenOfSpades());
        }

    }

    @Nested
    public class TestIsFlush{


        @Test
        public void TestIsFlushWithNoFlush(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('H', 4),
                            new PlayingCard('D', 3),
                            new PlayingCard('H', 7)));
            HandOfCards hand = new HandOfCards(cards);

            assertFalse(hand.isFlush());
        }

        @Test
        public void TestIsFlushWithHeartsFlush(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('H', 8),
                            new PlayingCard('H', 3),
                            new PlayingCard('H', 4),
                            new PlayingCard('H', 5),
                            new PlayingCard('H', 6)));
            HandOfCards hand = new HandOfCards(cards);

            assertTrue(hand.isFlush());
        }

        @Test
        public void TestIsFlushWithDiamondsFlush(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('D', 8),
                            new PlayingCard('D', 3),
                            new PlayingCard('D', 4),
                            new PlayingCard('D', 5),
                            new PlayingCard('D', 6)));
            HandOfCards hand = new HandOfCards(cards);

            assertTrue(hand.isFlush());
        }

        @Test
        public void TestIsFlushWithClubsFlush(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('C', 8),
                            new PlayingCard('C', 3),
                            new PlayingCard('C', 4),
                            new PlayingCard('C', 5),
                            new PlayingCard('C', 4)));
            HandOfCards hand = new HandOfCards(cards);

            assertTrue(hand.isFlush());
        }

        @Test
        public void TestIsFlushWithSpadesFlush(){
            ArrayList<PlayingCard> cards = new ArrayList<>(
                    Arrays.asList(new PlayingCard('S', 8),
                            new PlayingCard('S', 3),
                            new PlayingCard('S', 4),
                            new PlayingCard('S', 5),
                            new PlayingCard('S', 4)));
            HandOfCards hand = new HandOfCards(cards);

            assertTrue(hand.isFlush());
        }
    }
}
