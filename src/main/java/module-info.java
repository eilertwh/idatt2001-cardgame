module edu.ntnu.idatt2001.eilertwh {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;


    opens edu.ntnu.idatt2001.eilertwh to javafx.fxml;
    exports edu.ntnu.idatt2001.eilertwh;
}