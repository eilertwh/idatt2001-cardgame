package edu.ntnu.idatt2001.eilertwh;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Card app.
 * @author Eilert Werner Hansen
 * @version 0.1
 */
public class CardApp extends Application {
    /**
     * Override start method
     * used to set scene, resolution, title and icon of the program
     *
     * @param stage the stage of the program
     */
    @Override
    public void start(Stage stage) {

        FXMLLoader fxmlLoader = new FXMLLoader(CardApp.class.getClassLoader().getResource("CardGame.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(),600,400);
        } catch (IOException e) {
            e.printStackTrace();
        }

        PlayingCard iconCard = new PlayingCard('S',1);

        stage.getIcons().add(new Image(iconCard.getAsPng()));
        stage.setTitle("Card game");


        stage.setMaxHeight(400);
        stage.setMaxWidth(600);
        stage.setMinWidth(600);
        stage.setMinHeight(400);

        stage.setScene(scene);
        stage.show();

    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch();
    }
}

