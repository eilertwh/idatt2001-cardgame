package edu.ntnu.idatt2001.eilertwh;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * Card game controller.
 * @author Eilert Werner Hansen
 * @version 0.1
 */
public class CardGameController implements Initializable {

    private final ArrayList<PlayingCard> startCards = new ArrayList<>(
            Arrays.asList(new PlayingCard('S',10),
                    new PlayingCard('S',11),
                    new PlayingCard('S',12),
                    new PlayingCard('S',13),
                    new PlayingCard('S',1)));
    private HandOfCards currentHand = new HandOfCards(startCards);


    @FXML
    private ImageView cardFive = new ImageView();

    @FXML
    private ImageView cardFour = new ImageView();

    @FXML
    private ImageView cardOne = new ImageView();

    @FXML
    private ImageView cardThree = new ImageView();

    @FXML
    private ImageView cardTwo = new ImageView();

    @FXML
    private Text flushText;

    @FXML
    private Text queenOfSpadesText;

    @FXML
    private Text heartText;

    @FXML
    private Text sumText;

    /**
     * Deals out card to hand
     */
    @FXML
    void deal() {
        DeckOfCards deck = new DeckOfCards();
        try {
            currentHand = new HandOfCards(deck.dealHand(5));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        defaultText();
        showCards(currentHand);
    }

    /**
     * Checks hand for sum heart cards, Queen of spades and flush
     */
    @FXML
    void check() {

        String sum = "♦ Sum: " + currentHand.getSum();

        StringBuilder hearts = new StringBuilder("♥ No heart cards");
        if (currentHand.getSuitCards('H').size() > 0){
            hearts = new StringBuilder("♥ Heart cards:");
            for (int i = 0; i <currentHand.getSuitCards('H').size(); i++) {
                hearts.append(" ").append(currentHand.getSuitCards('H').get(i).getAsString());
            }
        }

        String queenOfSpades = "♠ Doesn't contain Queen of Spades";
        if (currentHand.containsQueenOfSpades()) queenOfSpades = "♠ Contains Queen of Spades";

        String flush = "♣ Is not flush";
        if (currentHand.isFlush()) flush = "♣ Is FLUSH!";


        sumText.setText(sum);
        heartText.setText(hearts.toString());
        queenOfSpadesText.setText(queenOfSpades);
        flushText.setText(flush);

    }

    /**
     * shows hand in GUI
     * @param hand hand to be shown
     */
    private void showCards(HandOfCards hand){
        ArrayList<PlayingCard> cards = hand.getCards();
        cardOne.setImage(new Image(cards.get(0).getAsPng()));
        cardTwo.setImage(new Image(cards.get(1).getAsPng()));
        cardThree.setImage(new Image(cards.get(2).getAsPng()));
        cardFour.setImage(new Image(cards.get(3).getAsPng()));
        cardFive.setImage(new Image(cards.get(4).getAsPng()));
    }

    /**
     * sets text to default
     */
    private void defaultText(){
        sumText.setText("♦");
        heartText.setText("♥");
        queenOfSpadesText.setText("♠");
        flushText.setText("♣");
    }

    /**
     * Initializes the scene
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        showCards(currentHand);
        defaultText();
    }
}
