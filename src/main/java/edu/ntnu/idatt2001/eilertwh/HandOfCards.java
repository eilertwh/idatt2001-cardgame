package edu.ntnu.idatt2001.eilertwh;


import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * represents a hand of cards.
 * can be used to store and check attributes of the hand
 *
 * @author Eilert Werner Hansen
 * @version 0.1
 */
public class HandOfCards {

    private final ArrayList<PlayingCard> cards;

    /**
     * Instantiates a new Hand of cards.
     *
     * @param cards the cards of the hand
     */
    public HandOfCards(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     * Gets cards.
     *
     * @return the cards
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Calculates the sum of all faces of the cards
     *
     * @return the sum of faces
     */
    public int getSum(){
        return cards.stream()
                .map(c -> c.getFace())
                .reduce(0, (a, b) -> a + b);
    }

    /**
     * get every card of a suit from the hand
     *
     * @param suit the suit of the cards you want to get
     * @return an array list with every card with the suit
     */
    public ArrayList<PlayingCard> getSuitCards(char suit){
        return cards.stream()
                .filter(c -> c.getSuit()==suit)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * checks if hand contains the Queen of spades playing card.
     *
     * @return true if the hand contains the Queen of spades, false if not
     */
    public boolean containsQueenOfSpades(){
        return cards.stream().anyMatch(c -> c.getAsString().equals("S12"));
    }

    /**
     * checks if the hand is a Flush
     *
     * @return true if the hand is a Flush, false if not
     */
    public boolean isFlush() {
        return cards.stream()
                .map(c -> c.getSuit())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream().anyMatch(element -> element.getValue() >= 5);
    }

}
