package edu.ntnu.idatt2001.eilertwh;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Represents a deck of cards.
 * when initialized it creates 52 cards with 13 of 4 suits
 *
 * @author Eilert Werner Hansen
 * @version 0.1
 */
public class DeckOfCards {

    private final HashMap<String, PlayingCard> cards;
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private final int numberOfFaces = 13;

    /**
     * Instantiates a new Deck of cards.
     */
    public DeckOfCards() {
        cards = new HashMap<>();
        for (int i = 1; i <=numberOfFaces; i++) {
            cards.put((String.format("%s%s", suit[0], i)),new PlayingCard(suit[0],i));
            cards.put((String.format("%s%s", suit[1], i)),new PlayingCard(suit[1],i));
            cards.put((String.format("%s%s", suit[2], i)),new PlayingCard(suit[2],i));
            cards.put((String.format("%s%s", suit[3], i)),new PlayingCard(suit[3],i));
        }
    }

    /**
     * return an Array list with random cards from the deck
     *
     * @param n the number of cards you want to deal out
     * @return Array list with n amount random cards from the deck
     * @throws IllegalAccessException if n is 0 , negative or bigger than the amount of cards in the deck
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalAccessException {
        if (n==0) throw new IllegalAccessException("you cant deal an empty hand");
        if (n<0) throw new IllegalAccessException("you cant have a negative amount of cards");
        if (n> cards.size()) throw new IllegalAccessException("not enough cards in deck");
        ArrayList<PlayingCard> hand = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i <n; i++) {
            ArrayList<PlayingCard> valuesList = new ArrayList<>(cards.values());
            int randomNumber = random.nextInt(cards.size());
            PlayingCard randomCard = valuesList.get(randomNumber);
            hand.add(randomCard);
            cards.remove(randomCard.getAsString());
        }
        return hand;
    }
}
